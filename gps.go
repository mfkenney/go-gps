// The gps package provides an interface to NMEA GPS units.
package gps

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/mfkenney/go-nmea"
	"github.com/tarm/serial"
)

// GPS data record
type Record struct {
	// Record time-stamp
	T time.Time `json:"t"`
	// Latitude in degrees
	Lat float64 `json:"lat"`
	// Longitude in degrees
	Lon float64 `json:"lon"`
	// Fix status
	Status string `json:"status"`
	// Number of satellites in view
	Nsat uint `json:"nsat"`
	// Horizontal dilution of precision
	Hdop float32 `json:"hdop"`
	// Speed in knots
	Speed float32 `json:"speed"`
	// Course in degrees True
	Course float32 `json:"course"`
}

const time_format = "020106 150405"

// Communication interface
type Gps struct {
	rdr   io.Reader
	wtr   io.Writer
	eol   string
	debug bool
	wg    sync.WaitGroup
}

func (r *Record) MarshalCsv() ([]string, error) {
	cols := make([]string, 8)
	cols[0] = strconv.FormatInt(r.T.Unix(), 10)
	cols[1] = strconv.FormatFloat(r.Lat, 'f', 6, 64)
	cols[2] = strconv.FormatFloat(r.Lon, 'f', 6, 64)
	cols[3] = r.Status
	cols[4] = strconv.FormatUint(uint64(r.Nsat), 10)
	cols[5] = strconv.FormatFloat(float64(r.Hdop), 'f', 1, 32)
	cols[6] = strconv.FormatFloat(float64(r.Speed), 'f', 1, 32)
	cols[7] = strconv.FormatFloat(float64(r.Course), 'f', 1, 32)

	return cols, nil
}

func (r *Record) ToCsv(w io.Writer, writeHeader bool) error {
	if writeHeader {
		fmt.Fprintln(w, "time,latitude,longitude,status,nsat,hdop,speed,course")
	}
	cols, _ := r.MarshalCsv()
	_, err := fmt.Fprintln(w, strings.Join(cols, ","))
	return err
}

func (r *Record) UnmarshalCsv(cols []string) error {
	var (
		ix  int64
		ux  uint64
		fx  float64
		err error
	)

	ix, err = strconv.ParseInt(cols[0], 10, 64)
	if err != nil {
		return err
	}
	r.T = time.Unix(ix, 0)

	r.Lat, err = strconv.ParseFloat(cols[1], 64)
	if err != nil {
		return err
	}

	r.Lon, err = strconv.ParseFloat(cols[2], 64)
	if err != nil {
		return err
	}

	r.Status = cols[3]

	ux, err = strconv.ParseUint(cols[4], 10, 64)
	if err != nil {
		return err
	}
	r.Nsat = uint(ux)

	fx, err = strconv.ParseFloat(cols[5], 32)
	if err != nil {
		return err
	}
	r.Hdop = float32(fx)

	fx, err = strconv.ParseFloat(cols[6], 32)
	if err != nil {
		return err
	}
	r.Speed = float32(fx)

	fx, err = strconv.ParseFloat(cols[7], 32)
	if err != nil {
		return err
	}
	r.Course = float32(fx)

	return nil
}

func conv_float(s string) float32 {
	f, err := strconv.ParseFloat(s, 32)
	if err != nil {
		return -1.
	}
	return float32(f)
}

func conv_uint(s string, base int) uint {
	u, err := strconv.ParseUint(s, base, 32)
	if err != nil {
		return 0
	}
	return uint(u)
}

// Scanner split function for NMEA data records
func scanRecord(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	// Search for starting '$'
	var start int
	for start = 0; start < len(data); start += 1 {
		if data[start] == '$' {
			break
		}
	}

	// Search for terminating linefeed
	for i := start; i < len(data); i++ {
		if data[i] == '\n' {
			return i + 1, data[start:i], nil
		}
	}

	// If we're at EOF, we have a final, non-empty, non-terminated
	// record. Return it.
	if atEOF && len(data) > start {
		return len(data), data[start:], nil
	}

	// Request more data.
	return start, nil, nil
}

// NewGps returns a new Gps communication interface attached to an
// I/O port. The device is initialized to return GPRMC and GPGGA NMEA
// sentences.
func NewGps(port io.ReadWriter) *Gps {
	g := Gps{eol: "\r\n"}
	g.rdr = port
	g.wtr = port

	// Disable all sentences and enable just GPRMC and GPGGA. This will
	// only work on Garmin GPS units, others will ignore it.
	time.Sleep(time.Second)
	g.Send("$PGRMO,,2\r\n")
	time.Sleep(time.Second)
	tags := []string{"GPRMC", "GPGGA"}
	for _, tag := range tags {
		g.Send(fmt.Sprintf("$PGRMO,%s,1\r\n", tag))
		time.Sleep(time.Second)
	}

	return &g
}

// Parse decimal degrees from the string representation of
// degrees and decimal minutes
func dm_to_deg(deg, dm, hemi string) float64 {
	ideg, err := strconv.ParseUint(deg, 10, 16)
	if err != nil {
		return 0.
	}
	min, err := strconv.ParseFloat(dm, 64)
	if err != nil {
		return 0.
	}

	var sign float64
	if hemi == "W" || hemi == "S" {
		sign = -1
	} else {
		sign = 1
	}

	return (float64(ideg) + min/60.) * sign
}

// Create a new Record from the contents of a GPRMC and GPGGA
// NMEA sentence.
func ParseRecord(rmc, gga *nmea.Sentence) *Record {
	t, _ := time.Parse(time_format,
		fmt.Sprintf("%s %s", rmc.Fields[8], rmc.Fields[0]))
	return &Record{
		T:      t,
		Lat:    dm_to_deg(rmc.Fields[2][0:2], rmc.Fields[2][2:], rmc.Fields[3]),
		Lon:    dm_to_deg(rmc.Fields[4][0:3], rmc.Fields[4][3:], rmc.Fields[5]),
		Status: rmc.Fields[1],
		Nsat:   conv_uint(gga.Fields[6], 10),
		Hdop:   conv_float(gga.Fields[7]),
		Speed:  conv_float(rmc.Fields[6]),
		Course: conv_float(rmc.Fields[7]),
	}
}

// Enable or disable debugging output.
func (g *Gps) SetDebug(state bool) {
	g.debug = state
}

// Send sends a string to the device.
func (g *Gps) Send(cmd string) error {
	if g.debug {
		log.Printf("<%q\n", cmd)
	}

	_, err := g.wtr.Write([]byte(cmd))
	if err == nil && !strings.HasSuffix(cmd, g.eol) {
		g.wtr.Write([]byte(g.eol))
	}
	return err
}

// Read a single NMEA sentence from the device.
func (g *Gps) Recv() ([]byte, error) {
	// Ensure we don't get stale data
	if v, ok := g.rdr.(*serial.Port); ok {
		v.Flush()
	}
	return bufio.NewReader(g.rdr).ReadBytes('\n')
}

// Poll returns a single GPS data record. A data record is constructed from
// a GPRMC and a GPGGA NMEA sentence.
func (g *Gps) Poll() (*Record, error) {
	var (
		err    error
		raw    []byte
		s, rmc *nmea.Sentence
	)

	// Ensure we don't get stale data
	if v, ok := g.rdr.(*serial.Port); ok {
		v.Flush()
	}

	scanner := bufio.NewScanner(g.rdr)
	scanner.Split(scanRecord)
	for scanner.Scan() {
		raw = scanner.Bytes()
		if g.debug {
			log.Printf(">%q\n", raw)
		}

		s, err = nmea.ParseSentence(bytes.TrimRight(raw, " \r\n"))
		if err != nil {
			return nil, err
		}

		// We need a GPRMC and GPGGA record with the same
		// timestamp.
		switch s.Id {
		case "GPRMC":
			rmc = s
		case "GPGGA":
			if rmc != nil && rmc.Fields[0] == s.Fields[0] {
				return ParseRecord(rmc, s), nil
			}
		}
	}

	return nil, fmt.Errorf("Timeout error")
}

// Stream sends Records to a channel until a timeout or until cancelled
func (g *Gps) Stream(ctx context.Context) <-chan *Record {
	out := make(chan *Record, 1)
	g.wg.Add(1)

	go func() {
		defer close(out)
		defer g.wg.Done()
		var (
			err    error
			s, rmc *nmea.Sentence
		)

		scanner := bufio.NewScanner(g.rdr)
		scanner.Split(scanRecord)
		for scanner.Scan() {
			raw := scanner.Bytes()
			select {
			case <-ctx.Done():
				return
			default:
			}

			s, err = nmea.ParseSentence(bytes.TrimRight(raw, " \r\n"))
			if err != nil {
				continue
			}

			// We need a GPRMC and GPGGA record with the same
			// timestamp.
			switch s.Id {
			case "GPRMC":
				rmc = s
			case "GPGGA":
				if rmc != nil && rmc.Fields[0] == s.Fields[0] {
					out <- ParseRecord(rmc, s)
				}
			}
		}
	}()

	return out
}

// Wait for the Stream to finish after it is cancelled
func (g *Gps) StreamWait() {
	g.wg.Wait()
}
