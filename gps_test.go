package gps

import (
	"bytes"
	"context"
	"math"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"
)

var sentences = []string{
	"jkljl\xff$GPRMC,194253,A,4739.3051,N,12218.9538,W,000.0,000.0,291012,018.2,E,A*02\r\n",
	"$GPGGA,194253,4739.3051,N,12218.9538,W,1,04,5.2,,M,-18.4,M,,*54\r\nbvcgsghew",
}

type rwbuffer struct {
	rbuf *bytes.Buffer
	wbuf *bytes.Buffer
}

func (b *rwbuffer) Read(p []byte) (int, error) {
	return b.rbuf.Read(p)
}

func (b *rwbuffer) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

func relerr(x, y float64) float64 {
	return math.Abs(y-x) / y
}

func helperOpenFile(t *testing.T, name string) *os.File {
	path := filepath.Join("testdata", name)
	f, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	return f
}

func TestSample(t *testing.T) {
	resp := []byte(strings.Join(sentences, ""))
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	g := NewGps(&rw)
	g.SetDebug(true)
	rec, err := g.Poll()
	if err != nil {
		t.Fatalf("Read error: %v", err)
	}

	if rec.Status != "A" {
		t.Errorf("Bad status: %v", rec.Status)
	}

	if rec.T.Year() != 2012 {
		t.Errorf("Bad timestamp: %v", rec)
	}

	if relerr(rec.Lat, 47.655085) > 0.000001 {
		t.Errorf("Latitude error: %f", rec.Lat-47.655085)
	}

	if relerr(rec.Lon, -122.315897) > 0.000001 {
		t.Errorf("Longitude error: %f", rec.Lon-(-122.315897))
	}

	if rec.Nsat != 4 {
		t.Errorf("Bad satellite count: %v", rec)
	}
}

func TestCsv(t *testing.T) {
	input := strings.Split("1458259200,73.127228,-149.441858,A,12,0.7,0.1,0.0", ",")
	tref, err := time.Parse(time.RFC3339, "2016-03-18T00:00:00Z")
	if err != nil {
		t.Fatal(err)
	}

	rec := &Record{}
	err = rec.UnmarshalCsv(input)
	if err != nil {
		t.Fatalf("Unmarshal error: %v", err)
	}

	if !tref.Equal(rec.T) {
		t.Fatalf("Timestamp mismatch: %q != %q", rec.T, tref)
	}

	output, _ := rec.MarshalCsv()
	for i, x := range output {
		if x != input[i] {
			t.Logf("Col[%d] mismatch: %v != %v", i, x, input[i])
		}
	}
}

func TestMultiRecord(t *testing.T) {
	f := helperOpenFile(t, "session.log")
	g := NewGps(f)
	count := int(0)
	valid := int(0)

	ch := g.Stream(context.Background())
	for rec := range ch {
		count++
		if rec.Status == "A" {
			valid++
		}
	}

	if count != 33 {
		t.Errorf("Bad record count: %d", count)
	}

	if valid != 33 {
		t.Errorf("Bad valid record count: %d", valid)
	}
}
